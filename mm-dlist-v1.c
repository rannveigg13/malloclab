/*
 * mm.c - Pseudo-code for a basic first-fit implicit free list memory allocator implementation
 * 
 * In this first pseudo-implementation we are using a implicit free list
 * where each block has a header and a footer, so we can traverse the list in both directions.
 * 
 * Later, we are going to compare different ways of coalescing, placing allocated blocks and
 * minimizing internal- and external fragmentation.
 *
 * NOTE TO STUDENTS: Replace this header comment with your own header
 * comment that gives a high level description of your solution.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in below _AND_ in the
 * struct that follows.
 *
 * === User information ===
 * Group: Un(as)signed
 * User 1: atlie13 
 * SSN: 010592-3489
 * User 2: rannveigg13
 * SSN: 121288-2659
 * === End User Information ===
 ********************************************************/
team_t team = {
    /* Group name */
    "Un(as)signed",
    /* First member's full name */
    "Atli Freyr Einarsson",
    /* First member's email address */
    "atlie13@ru.is",
    /* Second member's full name (leave blank if none) */
    "Rannveig Guðmundsdóttir",
    /* Second member's email address (leave blank if none) */
    "rannveigg13@ru.is",
    /* Leave blank */
    "",
    /* Leave blank */
    ""
};

/* single word (4) or double word (8) alignment */
#define W 4
#define D 8
#define HEAP (1<<11)
#define EXTRA (1<<9)
#define HSIZE 12

#define MAX(x, y) ((x) > (y)? (x) : (y))

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + HSIZE + (D-1)) & ~0x7)

#define PACK(size, alloc) ((size) | (alloc))

#define GET(p)      (*(size_t *)(p))
#define PUT(p, val) (*(size_t *)(p) = (val))

#define GET_SIZE(p) (GET(p) & ~0x7)
#define GET_ALLOC(p) (GET(p) & 0x1)
#define GET_PREV(p) (GET((char *)(p) + W))
#define GET_NEXT(p) (GET((char *)(p) + D))

#define HDRP(p) ((char *)(p) - HSIZE)

// Global variable for pointer to beginnig of heap
static char *alloc_ptr;
static char *free_ptr;

// Helper functions
static void *extend_heap(size_t words);
static void place(void *bp, size_t asize);
static void *find_fit(size_t asize);
static void *coalesce(void *bp);

/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
    // creating heap base structure
    if ((free_ptr = mem_sbrk()) == NULL) {
        return -1;
    }
    PUT(heap_ptr, 0);
    PUT(heap_ptr+W, PACK(HFSIZE,1)); // prologue header
    PUT(heap_ptr+D, PACK(HFSIZE,1)); // prologue footer
    PUT(heap_ptr+W+D, PACK(0,1)); // epilogue header/footer
    heap_ptr += D;

    // call sbrk to extend heap space
    // if heap reaches the end of the virtual memory, return -1
    // otherwise, return 0
    return 0;
}

void *mm_malloc(size_t size)
{
    // This function should return a pointer to an address on the heap, 
    // where there is sufficient free space for the payload itself along
    // with a header and footer.
    //
    // For starters, we'll be using the first-fit algorithm to return the 
    // first sufficient free block.
    
    // adjusted size with header and footer included
    size_t asize;
    // size to extend the heap
    size_t esize;
    // pointer to the block
    char *bp;
    
    if (size <= 0) {
        return NULL;
    }

    // Minimal size of block, 16 bytes
    if (size <= D) { // if payload is less than 8 bytes
        asize = HFSIZE + D;
    }
    // Otherwise, add 8 bytes for header+footer and align to 8 bytes
    else {
        asize = ALIGN(size);
    }
    
    // Find a fit for asize and save to bp
    if ((bp = find_fit(asize)) != NULL) {
        // Place asize block into memory pointed to by bp
        place(bp, asize);
        return bp;
    }

    // Maybe coalesce here?
    //coalesce(bp);

    // No fit found
    esize = MAX(asize+EXTRA, HEAP);
    if ((bp = extend_heap(esize / W)) == NULL) {
        return NULL;
    }
    place(bp, asize);
    return bp;
}

void mm_free(void *ptr)
{
    // Changes allocated bit (LSB) in header and footer from 1 to 0.
    //
    // Coalescing:
    // We will compare intermediate coalescing to deferred coalescing 
    // in order to find out which one is better.

    // Save size of ptr
    size_t size = GET_SIZE(HDRP(ptr));
    // set allocated bit to 0 (f), in both header and footer
    PUT(HDRP(ptr), PACK(size, 0));
    PUT(FTRP(ptr), PACK(size, 0));
    coalesce(ptr);
}

void *mm_realloc(void *ptr, size_t size)
{
    // Reallocates memory for pointer 'ptr' because its size has changed to 'size'
    // If there is sufficient free space adjacent to current 'ptr' block for the new 'size',
    // then the block is moved and header/footer updated.
    // Otherwise, we will search all available free blocks until we find a fitting match,
    // then the block is copied to the new destination and the old block is 'free-d'.
    void *newp;
    size_t oldSize;

    // allocating size bytes
    if ((newp = mm_malloc(size)) == NULL) {
        printf("ERROR: mm_malloc failed in mm_realloc\n");
        exit(1);
    }
    // get size of ptr
    oldSize = GET_SIZE(HDRP(ptr));
    // if new size is less than old size (size is reducing)
    if (size < oldSize) {
        // reduces old size to new size
        oldSize = size;
    }
    // copy oldSize bytes from ptr to newp
    memcpy(newp, ptr, oldSize);
    // free the old block
    mm_free(ptr);
    return newp;
}

int mm_check() {
 
}

static void *extend_heap(size_t words) {
    char *bp;
    size_t size;

    // Take an even count of words to keep 8-byte alignment
    size = (words % 2) ? (words+1) * W : words * W;
    // call mem_sbrk to extend the heap
    if ((bp = mem_sbrk(size)) == (void *)-1) {
        return NULL;
    }
   
    // set header on newly extended heap 
    PUT(HDRP(bp), PACK(size, 0));
    // set footer on newly extended heap
    PUT(FTRP(bp), PACK(size, 0));
    // x
    // set new epilogue header
    PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 1));
    
    return bp;
}

static void place(void *bp, size_t asize) {
    size_t bpsize = GET_SIZE(HDRP(bp));

    // if we can fit another block in additition to asize
    if (bpsize >= (asize + HFSIZE + D)) {
        // create allocated block of asize
        PUT(HDRP(bp), PACK(asize, 1));
        PUT(FTRP(bp), PACK(asize, 1));
        bp = NEXT_BLKP(bp);
        // create free block of excess size (bpsize-asize)
        PUT(HDRP(bp), PACK(bpsize-asize, 0));
        PUT(FTRP(bp), PACK(bpsize-asize, 0));
    }
    // fill up bp if we can't fit another block
    else {
        PUT(HDRP(bp), PACK(bpsize, 1));
        PUT(FTRP(bp), PACK(bpsize, 1));
    }
}

static void *find_fit(size_t asize) {
    void *bp;

    // first-fit algorithm 
    // *** TODO: optimize
    for (bp = heap_ptr; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp)) {
        if (!GET_ALLOC(HDRP(bp)) && (asize <= GET_SIZE(HDRP(bp)))) {
            return bp;
        }
    }
    return NULL;
}

static void *coalesce(void *bp) {
    size_t prev_alloc = GET_ALLOC(FTRP(PREV_BLKP(bp)));
    size_t prev_size = GET_SIZE(FTRP(PREV_BLKP(bp)));
    size_t size = GET_SIZE(HDRP(bp));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));
    size_t next_size = GET_SIZE(HDRP(NEXT_BLKP(bp)));

    // No adj. blocks free
    if (prev_alloc && next_alloc) {
        return bp;
    }
    // previous block free
    else if (!prev_alloc && next_alloc) {
        size += prev_size;
        // update footer
        PUT(FTRP(bp), PACK(size, 0));
        // move block pointer to previous block
        bp = PREV_BLKP(bp);
        // update header
        PUT(HDRP(bp), PACK(size, 0));
    }
    // next block free
    else if (prev_alloc && !next_alloc) {
        size += next_size;
        // update header
        PUT(HDRP(bp), PACK(size, 0));
        // update footer
        PUT(FTRP(bp), PACK(size, 0)); 
    }
    // previous and next blocks both free
    else {
        size += prev_size + next_size;
        // move block pointer to previous block
        bp = PREV_BLKP(bp);
        // update header
        PUT(HDRP(bp), PACK(size, 0));
        // update footer
        PUT(FTRP(bp), PACK(size, 0));
    }

    return bp;
}
