/*
 * mm.c - First implementation of a doubly-linked explicit list
 *
 * To begin with we implemented a first-fit implicit list, very similar to 
 * the one given in mm-firstfit.c. After understanding that first implementation,
 * we wrote this implementation of a doubly-linked explicit list:
 *
 * The heap is split into two seperate lists, a free-list and an allocated-list
 *
 * The block layout is as follows:
 * 4-bytes - 1-bit allocated/free + 2-bit empty + 29bit size of block
 * 4-bytes - link to previous block in same list (address)
 * 4-bytes - link to next block in same list (address)
 * Rest of block - PAYLOAD
 * => min. block size = 16 bytes
 * The doubly-linked list uses a first-fit algorithm, traversing the free-list to find free blocks.
 * When a block is free-d, we do intermediate coalescing to merge adjacent free blocks.
 * The code below is not fully functional (SEGFAULTs), we believe it is caused by pointer casting
 * or lack thereof.
 *
 *
 * The next step in our development, we are going to take a closer look at more complext data structures,
 * such as skiplists and segregated lists and try to implement them into our solution.
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in below _AND_ in the
 * struct that follows.
 *
 * === User information ===
 * Group: Un(as)signed
 * User 1: atlie13 
 * SSN: 010592-3489
 * User 2: rannveigg13
 * SSN: 121288-2659
 * === End User Information ===
 ********************************************************/
team_t team = {
    /* Group name */
    "Un(as)signed",
    /* First member's full name */
    "Atli Freyr Einarsson",
    /* First member's email address */
    "atlie13@ru.is",
    /* Second member's full name (leave blank if none) */
    "Rannveig Guðmundsdóttir",
    /* Second member's email address (leave blank if none) */
    "rannveigg13@ru.is",
    /* Leave blank */
    "",
    /* Leave blank */
    ""
};

/* single word (4) or double word (8) alignment */
#define W 4
#define D 8
#define HEAP (1<<11)
#define EXTRA (1<<9)
#define HSIZE 12
#define MINBLOCK 16

#define MAX(x, y) ((x) > (y)? (x) : (y))

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + HSIZE + (D-1)) & ~0x7)

#define PACK(size, alloc) ((size) | (alloc))

#define GET(p)      (*(size_t *)(p))
#define PUT(p, val) (*(size_t *)(p) = (val))

#define GET_SIZE(p) (GET(HDRS(p)) & ~0x7)
#define GET_ALLOC(p) (GET(HRDS(p)) & 0x1)
#define GET_PREV(p) (char *)(GET(HDRP(p)))
#define GET_NEXT(p) (char *)(GET(HDRN(p)))

#define HDRS(p) ((char *)(p) - HSIZE)
#define HDRP(p) ((char *)(p) - HSIZE+W)
#define HDRN(p) ((char *)(p) - HSIZE+D)

// Global variable for pointer to beginnig of heap
static char *alloc_ptr;
static char *free_ptr;

// Helper functions
static void *extend_heap(size_t words);
static void place(void *bp, size_t asize);
static void *find_fit(size_t asize);
static void *coalesce(void *bp);
static void *previous_free(void *ptr);
static void *previous_alloc(void *ptr);

/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
    // creating heap base structure
    if ((free_ptr = mem_sbrk(HEAP)) == NULL) {
        return -1;
    }
    PUT(free_ptr, PACK(HEAP, 0));
    PUT(free_ptr+W, 0); // prologue header
    PUT(free_ptr+D, 0); // prologue footer
    free_ptr += HSIZE;

    // call sbrk here?
    return 0;
}

void *mm_malloc(size_t size)
{
    // This function should return a pointer to an address on the heap, 
    // where there is sufficient free space for the payload itself along
    // with a header and footer.
    //
    // For starters, we'll be using the first-fit algorithm to return the 
    // first sufficient free block.
    
    // adjusted size with header and footer included
    size_t asize;
    // size to extend the heap
    size_t esize;
    // pointer to the block
    char *bp;
    
    if (size <= 0) {
        return NULL;
    }

    // Minimal size of block, 16 bytes
    if (size <= W) { // if payload is less than 4 bytes
        asize = HSIZE + W;
    }
    // Otherwise, add 12 bytes for header and align to 8 bytes
    else {
        asize = ALIGN(size);
    }
    
    // Find a fit for asize and save to bp
    if ((bp = find_fit(asize)) != NULL) {
        // Place asize block into memory pointed to by bp
        place(bp, asize);
        if (alloc_ptr == NULL) {
            alloc_ptr = bp;
        }
        return bp;
    }

    // Maybe coalesce here?
    //coalesce(bp);

    // No fit found
    esize = MAX(asize+EXTRA, HEAP);
    if ((bp = extend_heap(esize / W)) == NULL) {
        return NULL;
    }
    place(bp, asize);
    return bp;
}

void mm_free(void *ptr)
{
    // Changes allocated bit (LSB) in header and footer from 1 to 0
    // and fixes links in both lists.
    //
    // Coalescing:
    // We will compare intermediate coalescing to deferred coalescing 
    // in order to find out which one is better.

    // Save size of ptr
    size_t size = GET_SIZE(ptr);
    char *prev_alloc = GET_PREV(ptr);
    char *next_alloc = GET_NEXT(ptr);
    // fix allocated list
    PUT(HDRN(prev_alloc), (int)next_alloc);
    PUT(HDRP(next_alloc), (int)prev_alloc);
    // free block and add to free list
    PUT(HDRS(ptr), PACK(size, 0));
    char *prev_free = previous_free(ptr);
    char *next_free = GET_NEXT(prev_free);
    // connect current block to previous free
    PUT(HDRP(ptr), (int)prev_free);
    // connect current block to next free
    PUT(HDRN(ptr), (int)next_free);
    // connect previous free to current block
    PUT(HDRN(prev_free), (int)ptr);
    // connect next free to current block
    PUT(HDRP(next_free), (int)ptr);

    coalesce(ptr);
}

static void *previous_free(void *ptr) {
    void *bp;
    for (bp = free_ptr; &bp < &ptr; bp = GET_NEXT(bp)) {
    }
    return GET_PREV(bp);
}
static void *previous_alloc(void *ptr) {
    void *bp;
    for (bp = alloc_ptr; &bp < &ptr; bp = GET_NEXT(bp)) {
    }
    return GET_PREV(bp);
}

void *mm_realloc(void *ptr, size_t size)
{
    // Reallocates memory for pointer 'ptr' because its size has changed to 'size'
    // If there is sufficient free space adjacent to current 'ptr' block for the new 'size',
    // then the block is moved and header/footer updated.
    // Otherwise, we will search all available free blocks until we find a fitting match,
    // then the block is copied to the new destination and the old block is 'free-d'.
    void *newp;
    size_t oldSize;

    // allocating size bytes
    if ((newp = mm_malloc(size)) == NULL) {
        printf("ERROR: mm_malloc failed in mm_realloc\n");
        exit(1);
    }
    // get size of ptr
    oldSize = GET_SIZE(ptr);
    // if new size is less than old size (size is reducing)
    if (size < oldSize) {
        // reduces old size to new size
        oldSize = size;
    }
    // copy oldSize bytes from ptr to newp
    memcpy(newp, ptr, oldSize);
    // free the old block
    mm_free(ptr);
    return newp;
}

static void *extend_heap(size_t words) {
    char *bp;
    size_t size;

    // Take an even count of words to keep 8-byte alignment
    size = (words % 2) ? (words+1) * W : words * W;
    // call mem_sbrk to extend the heap
    if ((bp = mem_sbrk(size)) == (void *)-1) {
        return NULL;
    }
   
    char *prev_free = previous_free(bp);
    PUT(HDRS(bp), PACK(size, 0));
    PUT(HDRN(bp), 0);
    // connect previous free block to newly extended heap 
    PUT(HDRN(prev_free), (int)bp);
    // connect newly extended heap to previous free block
    PUT(HDRP(bp), (int)prev_free);
    
    return bp;
}

static void place(void *bp, size_t asize) {
    size_t bpsize = GET_SIZE(bp);
    char *prev_alloc = previous_alloc(bp);
    char *next_alloc = GET_NEXT(prev_alloc);

    // if we can fit another block in additition to asize
    if (bpsize >= (asize + MINBLOCK)) {
        // create allocated block of asize
        PUT(HDRS(bp), PACK(asize, 1));
        PUT(HDRP(bp), (int)prev_alloc);
        PUT(HDRN(bp), (int)next_alloc);
        PUT(HDRN(prev_alloc), (int)bp);
        PUT(HDRP(next_alloc), (int)bp);
        bp += asize;
        char *prev_free = previous_free(bp);
        char *next_free = GET_NEXT(prev_free);
        // create free block of excess size (bpsize-asize)
        PUT(HDRS(bp), PACK(bpsize-asize, 0));
        PUT(HDRP(bp), (int)prev_free);
        PUT(HDRN(bp), (int)next_free);
        PUT(HDRN(prev_free), (int)bp);
        PUT(HDRP(next_free), (int)bp);
    }
    // fill up bp if we can't fit another block
    else {
        PUT(HDRS(bp), PACK(bpsize, 1));
        PUT(HDRP(bp), (int)prev_alloc);
        PUT(HDRN(bp), (int)next_alloc);
        PUT(HDRN(prev_alloc), (int)bp);
        PUT(HDRP(next_alloc), (int)bp);
    }
}

static void *find_fit(size_t asize) {
    void *bp;

    // first-fit algorithm 
    // *** TODO: optimize
    for (bp = free_ptr; GET_NEXT(bp) != NULL; bp = GET_NEXT(bp)) {
        if (asize <= GET_SIZE(bp)) {
            return bp;
        }
    }
    return NULL;
}

static void *coalesce(void *bp) {
    void *prev_free = GET_PREV(bp);
    size_t prev_size = GET_SIZE(prev_free);
    size_t size = GET_SIZE(bp);
    void *next_free = GET_NEXT(bp);
    size_t next_size = GET_SIZE(next_free);
    int bp_addr = (int)bp;
    int prev_addr = (int)prev_free;
    int next_addr = (int)next_free;
    size_t prev_adj = bp_addr - prev_addr - prev_size;
    size_t next_adj = next_addr - bp_addr - size;

    // No free blocks adjacent
    if (prev_adj && next_adj) {
        return bp;
    }
    // previous free block adjacent
    else if (!prev_adj && next_adj) {
        size += prev_size;
        bp = prev_free;
        // update links and size
        PUT(HDRS(bp), PACK(size, 0));
        PUT(HDRN(bp), (int)next_free);
        PUT(HDRP(next_free), (int)bp);
    }
    // next free block adjacent
    else if (prev_adj && !next_adj) {
        char *nextnext = GET_NEXT(next_free);
        size += next_size;
        // update links and size
        PUT(HDRS(bp), PACK(size, 0));
        PUT(HDRN(bp), (int)nextnext);
        PUT(HDRP(nextnext), (int)bp);
    }
    // both prev. and next free blocks adjacent
    else {
        char *nextnext = GET_NEXT(next_free);
        size += prev_size + next_size;
        bp = prev_free;
        // update links and size
        PUT(HDRS(bp), PACK(size, 0));
        PUT(HDRN(bp), (int)nextnext);
        PUT(HDRP(nextnext), (int)bp);
    }

    return bp;
}
