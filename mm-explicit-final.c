/*
 * mm.c - Explicit Free-list Memory Allocator v.1.0
 * 
 * find_fit(size) uses first-fit placement algorithm.
 *
 * Coalescing is done immediately after free-ing a block or
 * extending the heap.
 *
 * Reallocating a block checks if the old block size is 
 * equivalent to 8-bit aligned reallocated size. If that 
 * is the case, it returns the old block.
 * Otherwise, a new block is allocated, the content is copied
 * over and the old block is free-d.
 * Additionally, we implemented an advanced reallocation algorithm
 * in the case where adjacent blocks are free, such that the block 
 * can grow/shrink in size. This would eliminate the need for calling
 * mm_malloc and give a lot better space utilization.
 * This requires copying overlapping areas on the heap, and in 
 * those cases we use memmove instead of memcpy.
 * Unfortunately, this implementation produces an error stating:
 * "ERROR : mm_realloc did not preserve the data from old block"
 * However, we did thorough debugging using CGDB. In the process
 * we compared the content of the old block and the new block and
 * it showed exactly the same content, which surprised us.
 * -> We were unable to resolve this and ended up commenting out
 * the "grow" section of mm_realloc.
 *
 * Heap Structure:
 * begin                                                     end
 *  -----------------------------------------------------------   
 * | pad | hdr | ftr | hdr | next | prev | ftr | CONTENT | hdr |
 *  -----------------------------------------------------------
 *    8  |  prologue |      prologue           |   user  | epi |
 *   bit | allocated |      freelist           |  alloc  | log |
 *
 * Allocated block layout:
 *  -------------------------------
 * | hdr(8:a) | PAYLOAD | ftr(8:a) |
 *  -------------------------------
 *
 * Free block layout:
 *  ---------------------------------------------
 * | hdr(8:a) | next | prev | GARBAGE | ftr(8:a) |
 *  ---------------------------------------------
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in below _AND_ in the
 * struct that follows.
 *
 * === User information ===
 * Group: Un(as)signed
 * User 1: atlie13 
 * SSN: 010592-3489
 * User 2: rannveigg13
 * SSN: 121288-2659
 * === End User Information ===
 ********************************************************/
team_t team = {
    /* Group name */
    "Un(as)signed",
    /* First member's full name */
    "Atli Freyr Einarsson",
    /* First member's email address */
    "atlie13@ru.is",
    /* Second member's full name (leave blank if none) */
    "Rannveig Guðmundsdóttir",
    /* Second member's email address (leave blank if none) */
    "rannveigg13@ru.is",
    /* Leave blank */
    "",
    /* Leave blank */
    ""
};

extern int verbose;

/* single word (4) or double word (8) alignment */
#define WSIZE 4
#define DSIZE 8
#define HEAP (1<<12)
#define ALOCHEAD 8
#define FREEHEAD 16

#define MAX(x, y) ((x) > (y)? (x) : (y))

/* Pack a size and allocated bit into a word */
#define PACK(size, alloc) ((size) | (alloc))

/* Read and write a word at address p */
#define GET(p)      (*(size_t *)(p))
#define LGET(p)     ((void *)(GET(p)))
#define PUT(p, val) (*(size_t *)(p) = ((size_t) val))

/* Read the size and allocated fields from address p */
#define GET_SIZE(p) (GET(p) & ~0x7)
#define GET_ALLOC(p) (GET(p) & 0x1)

/* Given block ptr bp, compute address of its header and footer */
#define HDRP(bp) ((char *)(bp) - WSIZE)
#define NLNK(bp) ((char *)(bp))
#define PLNK(bp) ((char *)(bp) + WSIZE)
#define FTRP(bp) ((char *)(bp) + GET_SIZE(HDRP(bp)) - DSIZE)

/* Given block ptr bp, compute address of next and previous blocks */
#define NEXT_BLKP(bp) ((char *)(bp) + GET_SIZE(((char *)(bp) - WSIZE)))
#define PREV_BLKP(bp) ((char *)(bp) - GET_SIZE(((char *)(bp) - DSIZE)))
 
/* Uncomment debug macro to use heap consistency checker */
//#define DEBUG
#ifdef DEBUG
#define CHECKHEAP printf("%s\n", __func__); mm_checkheap(2);
#endif
#ifndef DEBUG
#define CHECKHEAP
#endif

/* Global variables */
static char *heap_listp;
static char *free_list;

/* function prototypes for internal helper routines */
static void *extend_heap(size_t words);
static void place(void *bp, size_t asize);
static void *find_fit(size_t asize);
static void *coalesce(void *bp);
static void printblock(void *bp); 
static void checkblock(void *bp);
static void *previous_free(void *bp);
void mm_checkheap(int verbose);

/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
    /* creating heap base structure */
    if ((heap_listp = mem_sbrk(8*WSIZE)) == NULL)
        return -1;
    PUT(heap_listp, 0);                        /* alignment padding */
    PUT(heap_listp+WSIZE, PACK(ALOCHEAD, 1));  /* prologue header */ 
    PUT(heap_listp+DSIZE, PACK(ALOCHEAD, 1));  /* prologue footer */ 
    heap_listp += DSIZE;                       /* move to first block */
    free_list = heap_listp + DSIZE;            /* location of free */
    PUT(HDRP(free_list), PACK(16, 0));         /* free header */
    PUT(FTRP(free_list), PACK(16, 0));         /* free footer */
    PUT(NLNK(free_list), NULL);                /* free nextlink */
    PUT(PLNK(free_list), NULL);                /* free prevlink */
    PUT(free_list + FREEHEAD - WSIZE, PACK(0, 1)); /* epilogue */

    if (extend_heap(HEAP/WSIZE) == NULL) {
        CHECKHEAP;
        return -1;
    }
    CHECKHEAP;
    return 0;
}

/* 
 * mm_malloc - Allocate a block with at least size bytes of payload 
 */
void *mm_malloc(size_t size)
{
    CHECKHEAP;
    size_t asize;
    size_t esize;
    char *bp;
    
    if (size <= 0) {
        return NULL;
    }

    // Minimal size of block, 16 bytes
    if (size <= DSIZE) { // if payload is less than 8 bytes
        asize = ALOCHEAD + DSIZE;
    }
    // Otherwise, add 8 bytes for header+footer and align to 8 bytes
    else {
        asize = DSIZE * ((size + (ALOCHEAD) + (DSIZE-1)) / DSIZE);
    }
    
    // Find a fit for asize and save to bp
    if ((bp = find_fit(asize)) != NULL) {
        // Place asize block into memory pointed to by bp
        coalesce(bp);
        place(bp, asize);
        CHECKHEAP;
        return bp;
    }

    // No fit found
    esize = MAX(asize,HEAP);
    if ((bp = extend_heap(esize/WSIZE)) == NULL) {
        return NULL;
    }
    coalesce(bp);
    place(bp, asize);
    CHECKHEAP;
    return bp;
}

/* 
 * mm_free - Free a block 
 */
void mm_free(void *bp)
{
    CHECKHEAP;
    size_t size = GET_SIZE(HDRP(bp));

    void *prev = previous_free(bp);
    void *next = LGET(NLNK(prev));

    PUT(HDRP(bp), PACK(size, 0)); // Free header
    PUT(FTRP(bp), PACK(size, 0)); // Free footer
    PUT(NLNK(bp), next);  // set nextlink to next
    PUT(PLNK(bp), prev);  // set prevlink to prev
    if (prev) {
        PUT(NLNK(prev), bp);
    }
    if (next) {                    // if next block exists
        PUT(PLNK(next), bp); // set prevlink of next to this
    }
    CHECKHEAP;
    coalesce(bp);
    CHECKHEAP;
}

/*
 * mm_realloc - naive implementation of mm_realloc
 */
void *mm_realloc(void *ptr, size_t size)
{
    if (ptr == NULL) {
        return mm_malloc(size);
    }
    if (size == 0) {
        mm_free(ptr);
        return NULL;
    }
    size_t asize;
    if (size <= DSIZE) {
        asize = ALOCHEAD + DSIZE;
    }
    else {
        asize = DSIZE * ((size + (ALOCHEAD) + (DSIZE-1)) / DSIZE);
    }
    size_t osize = GET_SIZE(HDRP(ptr));
    if (osize == asize) {
        return ptr;
    }
	
	/* Realloc optimization - Algorithm to merge allocated block with adjacent free block(s)
	 * Unfortunately this doesn't work with mdriver, returns 
	 * "ERROR : mm_realloc did not preserve the data from old block" */
	/*
	size_t extra = 0;
    size_t excess = 0;
    void *prev = PREV_BLKP(ptr);
    void *next = NEXT_BLKP(ptr);
    size_t prev_alloc = GET_ALLOC(FTRP(prev));
    if (prev == free_list) {
        prev_alloc = 1;
    }
    size_t next_alloc = GET_ALLOC(HDRP(next));
    size_t prev_size = GET_SIZE(FTRP(prev));
    size_t next_size = GET_SIZE(HDRP(next));
    if (!prev_alloc && next_alloc) {
        void *prev_link = LGET(PLNK(prev));
        void *next_link = LGET(NLNK(prev));
        extra = prev_size;
        excess = osize + extra - asize;
        if (excess >= 16) {
            // place allocated and copy content
            PUT(HDRP(prev), PACK(asize, 1));
            PUT(FTRP(prev), PACK(asize, 1));
            prev = memmove(prev, ptr, osize);
            ptr = NEXT_BLKP(prev);
            // place free block
            PUT(HDRP(ptr), PACK(excess, 0));
            PUT(FTRP(ptr), PACK(excess, 0));
            // fix free links!!!
            PUT(PLNK(ptr), prev_link);
            if (prev_link) {
                PUT(NLNK(prev_link), ptr);
            }
            PUT(NLNK(ptr), next_link);
            if (next_link) {
                PUT(PLNK(next_link), ptr);
            }
            return prev;
        }
        else if (0 <= excess && excess < 16) {
            // place only
            PUT(HDRP(prev), PACK(asize+excess, 1));
            PUT(FTRP(prev), PACK(asize+excess, 1));
            prev = memmove(prev, ptr, osize);
            if (prev_link) {
                PUT(NLNK(prev_link), next_link);
            }
            if (next_link) {
                PUT(PLNK(next_link), prev_link);
            }
            return prev;
        }
    }
    if (prev_alloc && !next_alloc) {
        void *prev_link = LGET(PLNK(next));
        void *next_link = LGET(NLNK(next));
        extra = next_size;
        excess = osize + extra - asize;
        if (excess >= 16) {
            PUT(HDRP(ptr), PACK(asize, 1));
            PUT(FTRP(ptr), PACK(asize, 1));
            ptr = NEXT_BLKP(ptr);
            PUT(HDRP(ptr), PACK(excess, 0));
            PUT(FTRP(ptr), PACK(excess, 0));
            PUT(PLNK(ptr), prev_link);
            if (prev_link) {
                PUT(NLNK(prev_link), ptr);
            }
            PUT(NLNK(ptr), prev_link);
            if (next_link) {
                PUT(PLNK(next_link), ptr);
            }
            return PREV_BLKP(ptr);
        }
        else if (0 <= excess && excess < 16) {
            PUT(HDRP(ptr), PACK(asize+excess, 1));
            PUT(FTRP(ptr), PACK(asize+excess, 1));
            if (prev_link) {
                PUT(NLNK(prev_link), next_link);
            }
            if (next_link) {
                PUT(PLNK(next_link), prev_link);
            }
            return PREV_BLKP(ptr);
        }   
    }
    if (!prev_alloc && !next_alloc) {
        void *prev_link = LGET(PLNK(prev));
        void *next_link = LGET(NLNK(next));
        extra = next_size + prev_size;
        excess = osize + extra - asize;
        if (excess >= 16) {
            PUT(HDRP(prev), PACK(asize, 1));
            PUT(FTRP(prev), PACK(asize, 1));
            prev = memmove(prev, ptr, osize);
            ptr = NEXT_BLKP(prev);
            PUT(HDRP(ptr), PACK(excess, 0));
            PUT(FTRP(ptr), PACK(excess, 0));
            PUT(PLNK(ptr), prev_link);
            if (prev_link) {
                PUT(NLNK(prev_link), ptr);
            }
            PUT(NLNK(ptr), next_link);
            if (next_link) {
                PUT(PLNK(next_link), ptr);
            }
            return prev;
        }
        else if (0 <= excess && excess < 16) {
            PUT(HDRP(ptr), PACK(asize+excess, 1));
            PUT(FTRP(ptr), PACK(asize+excess, 1));
            prev = memmove(prev, ptr, osize);
            if (prev_link) {
                PUT(NLNK(prev_link), next_link);
            }
            if (next_link) {
                PUT(PLNK(next_link), prev_link);
            }
            return prev;
        }
    }*/
	
    void *newp;
    // allocating size bytes
    if ((newp = mm_malloc(size)) == NULL) {
        printf("ERROR: mm_malloc failed in mm_realloc\n");
        exit(1);
    }
    // if new size is less than old size (size is reducing)
    if (size < osize) {
        // reduces old size to new size
        osize = size;
    }
    // copy oldSize bytes from ptr to newp
    memcpy(newp, ptr, osize);
    // free the old block
    mm_free(ptr);
    return newp;
}

/* 
 * mm_checkheap - Check the heap for consistency 
 */
void mm_checkheap(int verbose) 
{
    char *bp = heap_listp;

    if (verbose)
        printf("Heap (%p):\n", heap_listp);

    if ((GET_SIZE(HDRP(heap_listp)) != DSIZE) || !GET_ALLOC(HDRP(heap_listp)))
        printf("Bad prologue header\n");
    checkblock(heap_listp);

    for (bp = heap_listp; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp)) {
        if (verbose) 
            printblock(bp);
        checkblock(bp);
    }
     
    if (verbose)
        printblock(bp);
    if ((GET_SIZE(HDRP(bp)) != 0) || !(GET_ALLOC(HDRP(bp))))
        printf("Bad epilogue header\n");
}

/* 
 * extend_heap - Extend heap with free block and return its block pointer
 */
static void *extend_heap(size_t words)
{
    char *bp;
    size_t size;
        
    /* Allocate an even number of words to maintain alignment */
    size = (words % 2) ? (words+1) * WSIZE : words * WSIZE;
    if ((bp = mem_sbrk(size)) == (void *)-1) 
        return NULL;

    /* Initialize free block header/footer and the epilogue header */
    PUT(HDRP(bp), PACK(size, 0));         /* free block header */
    void *prev = previous_free(bp);
    PUT(NLNK(bp), NULL);                /* next link points to null */
    PUT(PLNK(bp), prev);          /* previous link point to last */
    if (prev) {
        PUT(NLNK(prev), bp);
    }
    PUT(FTRP(bp), PACK(size, 0));         /* free block footer */
    PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 1)); /* new epilogue header */

    /* Coalesce if the previous block was free */
    void *result = coalesce(bp);
    return result;
}

/* 
 * place - Place block of asize bytes at start of free block bp 
 *         and split if remainder would be at least minimum block size
 */
static void place(void *bp, size_t asize)
{
    size_t csize = GET_SIZE(HDRP(bp));   

    // there is extra space for a free block
    if ((csize - asize) >= (DSIZE + ALOCHEAD)) { 
        void *prev = LGET(PLNK(bp));
        void *next = LGET(NLNK(bp));
        PUT(HDRP(bp), PACK(asize, 1));
        PUT(FTRP(bp), PACK(asize, 1));
        bp = NEXT_BLKP(bp);
        PUT(HDRP(bp), PACK(csize-asize, 0));
        PUT(FTRP(bp), PACK(csize-asize, 0));
        PUT(NLNK(bp), next);
        if (next) {
            PUT(PLNK(next), bp);
        }
        PUT(PLNK(bp), prev);
        if (prev) {
            PUT(NLNK(prev), bp);
        }
    } // no extra space for a free block
    else { 
        PUT(HDRP(bp), PACK(csize, 1));
        PUT(FTRP(bp), PACK(csize, 1));
        void *next = LGET(NLNK(bp));
        void *prev = LGET(PLNK(bp));
        if (prev) {
            PUT(NLNK(prev), next);
        }
        if (next) {
            PUT(PLNK(next), prev);
        }
    }
}

/* 
 * find_fit - Find a fit for a block with asize bytes 
 */
static void *find_fit(size_t asize)
{
    /* first fit search */
    void *bp;

    for (bp = LGET(NLNK(free_list)); NLNK(bp) != NULL; bp = LGET(NLNK(bp))) {
        if (asize <= GET_SIZE(HDRP(bp)) && !GET_ALLOC(HDRP(bp))) {
            return bp;
        }
    }
    return NULL; /* no fit */
}

/*
 * coalesce - boundary tag coalescing. Return ptr to coalesced block
 */
static void *coalesce(void *bp) 
{
    size_t prev_alloc = GET_ALLOC(FTRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));
    size_t size = GET_SIZE(HDRP(bp));
    
    if(PREV_BLKP(bp) == free_list) {
       prev_alloc = 1;
    }
    
    if (prev_alloc && next_alloc) {            /* Case 1 */
        return bp;
    }

    else if (prev_alloc && !next_alloc) {      /* Case 2 */
        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
        void *next = LGET(NLNK(NEXT_BLKP(bp)));
        PUT(HDRP(bp), PACK(size, 0));
        PUT(FTRP(bp), PACK(size, 0));
        PUT(NLNK(bp), next);
        if (next) {
            PUT(PLNK(next), bp);
        }
    }
	
    else if (!prev_alloc && next_alloc) {      /* Case 3 */
        size += GET_SIZE(HDRP(PREV_BLKP(bp)));
        PUT(FTRP(bp), PACK(size, 0));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        void *next = LGET(NLNK(bp));
        bp = PREV_BLKP(bp);
        PUT(NLNK(bp), next);
        if (next) {
            PUT(PLNK(next), bp);
        }
    }

    else {                                     /* Case 4 */
        size += GET_SIZE(HDRP(PREV_BLKP(bp))) + 
            GET_SIZE(FTRP(NEXT_BLKP(bp)));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        PUT(FTRP(NEXT_BLKP(bp)), PACK(size, 0));
        void *next = LGET(NLNK(NEXT_BLKP(bp)));
        bp = PREV_BLKP(bp);
        PUT(NLNK(bp), next);
        if (next) {
            PUT(PLNK(next), bp);
        }
    }

    return bp;
}

/*
 * printblock - prints header and footer of block at bp
 */
static void printblock(void *bp) 
{
    size_t hsize, halloc, fsize, falloc;

    hsize = GET_SIZE(HDRP(bp));
    halloc = GET_ALLOC(HDRP(bp));  
    fsize = GET_SIZE(FTRP(bp));
    falloc = GET_ALLOC(FTRP(bp));  
    
    if (hsize == 0) {
        printf("%p: EOL\n", bp);
        return;
    }

    printf("%p: header: [%d:%c] footer: [%d:%c]\n", bp, 
           hsize, (halloc ? 'a' : 'f'), 
           fsize, (falloc ? 'a' : 'f')); 
}

/*
 * checkblock - checks for alignment and if header matches footer
 */
static void checkblock(void *bp) 
{
    if ((size_t)bp % 8)
        printf("Error: %p is not doubleword aligned\n", bp);
    if (GET(HDRP(bp)) != GET(FTRP(bp)))
        printf("Error: header does not match footer\n");
}

/*
 * previous_free - finds closest previous free block on heap
 */
static void *previous_free(void *bp)
{
    void *ptr;
    for (ptr = PREV_BLKP(bp); ptr != heap_listp; ptr = PREV_BLKP(ptr)) {
        if(!GET_ALLOC(HDRP(ptr))) {
            return ptr;
        }
    }
    return NULL;
}
